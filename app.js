var express = require('express');
var exportToExcel = require('export-to-excel');
var parseXlsx = require('excel');
var csvWriter = require('csv-write-stream');
var fs = require('fs');
var app = express();

app.get('/', function (req, res) {
    var arr = [];
    console.log("Excel Parsing");
    parseXlsx('Rapunzels-Secret.xlsx', function(err, data) {
        if(err) throw err;
        else{
            for(var i=0;i<data.length;i++){
                if(data[i][2]){
                    arr.push({name : data[i][1], mobile : data[i][2],address : data[i][3]});
                }
            }
            console.log("Total Data ="+arr.length);

            //var writer = csvWriter({ headers: ["Mobile No"]})
            //writer.pipe(fs.createWriteStream('out.csv'))
            //writer.write([number]);
            //writer.end()
            exportToExcel.exportXLSX({
                filename: 'createOutput',
                sheetname: 'Customer Data',
                title: [
                    {
                        "fieldName": "name",
                        "displayName": "Name",
                        "cellWidth": 30
                    },
                    {
                        "fieldName": "mobile",
                        "displayName": "Mobile No",
                        "cellWidth": 15,
                        "type": "string"
                    },
                    {
                        "fieldName": "address",
                        "displayName": "Location",
                        "cellWidth": 15
                    }
                ],
                data: arr
            })

        }

    });
});

app.listen(3001, function () {
    console.log("Server is running on port 3001");

});
